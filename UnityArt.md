---
version: 2
titre: Developement d'une installation artistique avec Unity
type de projet: Projet de semestre 6
année scolaire: 2021/2021
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1
mots-clés: [Unity, Art, Analyse Video]
langue: [F,E,D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.5\textwidth]{images/Art.png}
\end{center}
```

## Description/Contexte
Dans le cadre des 125 ans de l'école d'ingénieurs, iCoSys a développé plusieurs installations artistiques. 
Une de ces installation a été invité au [festival des lumières à morat](https://www.murtenlichtfestival.ch/), 
nommé [Human to plants](https://www.murtenlichtfestival.ch/_files/ugd/d709c7_7bec8b64b9634565b83550c7feb1d621.pdf).
Suite au succès de cette installation, l'école a été réinvité a participer à l'édition 2023 avec une nouvelle installation.
Ce projet réuni le développement de jeux vidéo, l’analyse de données et le défi technique d’une installation physique pour créer une plateforme basé sur Unity pour développer des installation artistiques interactives.
Dans le projet actuel, le moteur de jeux Unity est utilisé pour créer une scène virtuelle qui est affiché sur un ou plusieurs beamers.
Les visiteurs sont filmées avec une caméra et affiché dans la scène grâce a un outil nommé OpenPose.
Le plugin [OpenPose Unity Plugin](https://github.com/CMU-Perceptual-Computing-Lab/openpose_unity_plugin) intègre l’outil OpenPose dans Unity, ce qui permet d’utiliser le squelette des personnes dans le champ de vision de la camera pour en créer des objets 3D dans la scène Unity affiché.

Cette solution a plusieurs problèmes qui limitent son utilisation fortement :
- Le plugin utilisé n'est plus maintenu.
  - Ceci nous force a utiliser une ancienne version d'Unity et en plus ne permet pas d'utiliser les dernières versions d'OpenPose.
- Il n’y a pas de nettoyage et traitement d’erreures de la sortie de OpenPose
  - Notamment beaucoup de personnes sur l’image ou une qualité de video mauvaise ont un grand impact sur le résultat

Le projet a donc comme but de trouver une meilleure solution à ces problèmes, soit en mettant à jour le plugin actuel ou avec un nouveau développement, pour créer une base technique solide pour des futures projets.
Cette nouvelle solution sera testée dans un prototype d'une nouvelle installation artistique.
Basé sur les résultats de ce projet de semestre, un projet de Bachelor peut être fait (par la même personne ou pas) qui élabore et mets en place l'installation artistique finale qui sera presenté en janvier 2023.

## Objectifs/Tâches

- Prise en main de Unity
- Prise en main des outils de detection de personnes sur webcam
- Developement d'un plugin Unity
- Prototype d'une installation artistique
- Documentation du projet
