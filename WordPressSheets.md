---
version: 2
titre: Synchronisation entre WordPress et Google sheets
type de projet: Projet de semestre 6
année scolaire: 2021/2021
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1
mots-clés: [Wordpress, Developement Web]
langue: [F,E,D]
confidentialité: non
suite: non
proposé par étudiant: oui
attribué à: 
  - Jordan Jérémy
assistants:
  - Jonathan Donzallaz
---
```{=tex}
\begin{center}
\includegraphics[width=0.5\textwidth]{images/Wordpress.png}
\end{center}
```

## Description/Contexte
Wordpress est un CMS hautement populaire. D’une manière relativement simple, il est possible de créer des sites webs divers, comme par exemple une présence web pour un club sportif.
Même que l’administration d’un tel site est relativement simple et a besoin de peu de connaissances en informatique, la barrière d’entrée reste quand même suffisamment haute pour que par exemple des clubs sportifs préfèrent de s’organiser avec des feuilles Excels (typiquement partagé avec google sheets) pour organiser leur membres, présences et inscriptions à des compétitions.


A travers de ce projet de semestre nous allons explorer la possibilité de réunir ces deux mondes de fonctionnement. Le but central du projet est d’implémenter un moyen de synchroniser (uni ou bidirectionnel) les données d’un Google Sheet avec le contenu du site web Wordpress. Ceci dans le contexte d’un club sportif existant.


Durant le projet nous allons enumerer les besoins d’un tel système, explorer les possibilités techniques pour mettre en place ce type de synchronisation d’une manière la plus générique que possible, implémenter une telle synchronisation et la tester avec un prototype d’un site web pour un club sportif.



## Objectifs/Tâches

- Étude des APIs WordPress (developement de plugins etc.)
- Étude de l'API de Google Sheets
- Conception d'une solution de synchronisation entre WordPress et Google Sheets
- Implementation d'un site web WordPress qui utilise la synchronisation Google Sheets
- Documentation du projet
