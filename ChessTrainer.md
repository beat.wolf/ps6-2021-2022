---
version: 2
titre: Création automatique d’entraînements pour les échecs
type de projet: Projet de semestre 6
année scolaire: 2021/2021
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 1
assistants:
  - Yann-Ivain Beffa
mots-clés: [Application Web, Kubernetes, Cloud, Scaling]
langue: [F,E,D]
confidentialité: non
suite: non
proposé par étudiant: oui
attribué à:
   - Schroeter Maxime
---
```{=tex}
\begin{center}
\includegraphics[width=0.5\textwidth]{images/chess.png}
\end{center}
```

## Description/Contexte

Le jeu des échecs est devenu énormément populaire ces dernières années, notamment à travers de séries comme "The queens gambit".
Ce qui rend le jeu d’échecs intéressant c'est des règles relativement simples, mais une quantité de situations complexes énormes.
Pour apprendre a mieux jouer et être compétitif durant ces situations complexes, les puzzles d’échecs sont une manière populaire de s’entraîner en résolvant des situations spécifiques d’échecs pour trouver la meilleure solution.

Durant un premier projet de semestre, un outil Python a été développé pour créer des Puzzles individualisées selon l'historique de jeux d'un joueur en particulier.
Ce deuxième projet a comme but de créer une application web qui mets à disposition ces puzzles a des joueurs en ligne.

Le projet devra mettre en place non seulement un site web fonctionnel, mais aussi mettre en place une infrastructure scalable qui permet de s'adapter à un grand nombre d'utilisateurs.
En effet, la création des puzzles est relativement cher en temps de calcul, ce qui nécessite un design approprié pour le déploiement.
La consommation de ressources de la solution déployée sera analysée d’un point de vue hardware, électricité et couts.

Selon les besoins de l'application, la base du codé développé durant le premier projet de semestre sera étendu avec les fonctionnalités nécessaires. Ceci inclus l’amélioration de la génération de puzzles ou une meilleure classification de ces puzzles (difficulté etc.)


## Objectifs/Tâches

- Création de maquettes et un design d'un site web pour des puzzles d'echeques
- Documentation des libraries et outils permettant de créer un tel site
- Déploiement du prototype sur un site web accessible au grand public
- Documentation du projet
